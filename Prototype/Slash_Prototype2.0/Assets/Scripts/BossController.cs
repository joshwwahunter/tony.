﻿using UnityEngine;
using System.Collections;

public class BossController : MonoBehaviour {

    //Player detection variables
	public bool playerDetected;
    private bool playerInSight;
    public float enemyFOV = 90f;
    private SphereCollider enemyRangeCollider;
    public LayerMask mask = -1;


    //Stores player location - set to static to keep looking at last location - can be used for implement player search by enemy in future 
    public static GameObject gbjPlayer;

    //variables required for spawning arrows
    public GameObject arrowPrefab;
    private GameObject arrowClone;
    public GameObject arrowSpawner;
    public float arrowSpeed = 10;
    public float arrowlife = 3;
    private bool spawnArrows = false;

    //Offset for arrow spawn location on boss
    public Vector3 offset = new Vector3(0,3,15);

    void Awake()
    {
        enemyRangeCollider = gameObject.GetComponent<SphereCollider>();
    }

    // Use this for initialization
    void Start () {

		gameObject.GetComponentInChildren<MeshRenderer>().material.SetColor("_Color", Color.blue);
	}

	// Update is called once per frame
	void Update ()
    {

		if (playerDetected)
		{
            
            gameObject.GetComponentInChildren<MeshRenderer>().material.SetColor("_Color", Color.yellow);
            if (playerInSight)
            {
                gameObject.GetComponentInChildren<MeshRenderer>().material.SetColor("_Color", Color.red);
            }
        }
		else
		{
			gameObject.GetComponentInChildren<MeshRenderer>().material.SetColor("_Color", Color.blue);
		}



	}

    //Player radius enter trigger for Boss
	void OnTriggerStay(Collider other)
	{
        
		if (other.gameObject.tag == "Player")
		{
            gbjPlayer = other.gameObject;
            playerDetected = true;
            playerInSight = false;

            // Create a vector from the enemy to the player and store the angle between it and forward.
            Vector3 direction = other.transform.position - transform.position;
            float angle = Vector3.Angle(direction, transform.forward);

            
            

            // If the angle between forward and where the player is, is less than half the angle of view...
            if (angle < enemyFOV * 0.5f)
            {
                RaycastHit hit;

                // ... and if a raycast towards the player hits something...
                if (Physics.Raycast(transform.position + transform.up, direction.normalized, out hit, enemyRangeCollider.radius, mask))
                {
                    
                    // ... and if the raycast hits the player...
                    if (hit.collider.gameObject.tag == "Player")
                    {
                        Debug.DrawRay(transform.position + transform.up, direction.normalized * 10, Color.red);
                        //Debug.DrawLine(transform.position + transform.up, gbjPlayer.transform.position);
                        // ... the player is in sight.
                        playerInSight = true;

                        //Make boss look at player
                        gameObject.transform.LookAt(gbjPlayer.transform);

                        //Start spawning arrows by calling the attack iterator
                        if (!spawnArrows)
                        {
                            spawnArrows = true;
                            StartCoroutine(AttackPlayer());
                        }

                    }
                }
            }
        }
	}

	void OnTriggerExit(Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			playerDetected = false;
        }
	}


    //Iterator for ranged attack 
    IEnumerator AttackPlayer()
    {      
        //Spawn arrow every 1 sec
        yield return new WaitForSeconds(1f);

        arrowClone = (GameObject)Instantiate(arrowPrefab, arrowSpawner.transform.position, arrowSpawner.transform.rotation);

        //Shooting arrow along player direction vector
        arrowClone.GetComponent<Rigidbody>().velocity = ((gbjPlayer.transform.position - transform.position).normalized) * arrowSpeed;

        Destroy(arrowClone, arrowlife);

        spawnArrows = false;
    }
}
