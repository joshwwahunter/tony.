﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : Unit
{

	//Player-Movement related variables
	public float jumpHeight;
	private Rigidbody rb;
	private Transform camContainer;
	private Camera playerCam;
	private Animator anim;
	private bool onAir = false;

	//Combo Hit Notifier
	public Text comboHitText;
	public int noOfHits = 0;
	public Animator textAnimator;
	public bool hitTextAnimate;
	public float comboTimer = 3f;

	//Combat related variables
	public int startHealth;
	public float respawnTime = 2.5f;

	//Bullet related variables
	public GameObject bulletPrefab;
	public Transform bulletSpawn;
	public float bulletSpeed;
	public float bulletLife;

	//Set variables for camera shake
	private Vector3 playerCamOriginalPos;
	private Vector3 playerCamShakePos;
	public float shakeAmount = 0.5f;
	public float shakeDuration = 1f;
	private float shakeTime;
	public bool shakeCamera = false;

	float mouseYDelta;
	float mouseXDelta;


	void Awake ()
	{
		rb = GetComponent<Rigidbody> ();
		anim = GetComponentInChildren<Animator> ();
		textAnimator = comboHitText.GetComponent<Animator> ();
		shakeTime = shakeDuration;
	}

	// Use this for initialization
	void Start ()
	{
		startHealth = health;
		playerCam = Camera.main;
		playerCamOriginalPos = playerCam.transform.localPosition;
		camContainer = playerCam.transform.parent;
		hitTextAnimate = false;
	}

	// Update is called once per frame
	void Update ()
	{

		if (OptionsController.controlType == 1) {
			mouseYDelta = Input.GetAxis ("Right Stick Y Axis");
			mouseXDelta = Input.GetAxis ("Right Stick X Axis");
		} else if (OptionsController.controlType == 2) {
			mouseYDelta = Input.GetAxis ("Mouse Y");
			mouseXDelta = Input.GetAxis ("Mouse X");
		}
		print (Input.GetAxis ("Right Stick Y Axis"));
		//Cursor.lockState = CursorLockMode.Locked;
		//Cursor.visible = false;

		camContainer.Rotate (-mouseYDelta, 0, 0);


		if (health <= 0)
			return; 
		
		float horizontal = Input.GetAxis ("Horizontal");
		float vertical = Input.GetAxis ("Vertical");

		Vector3 input = new Vector3 (horizontal, 0, vertical);

		//We normalize the input vector, so that it always has a size of 1 (even when both forward and sideways are pressed)
		Vector3 vel = input.normalized * speed;

		anim.SetFloat ("HorizontalSpeed", input.x);
		anim.SetFloat ("VerticalSpeed", input.z);
		//We rotate the vector, so that it returns a global vector, based on the rotation of the character
		vel = transform.TransformVector (vel);
		//We set the vel.y, because otherwise the rigibodies y-velocity is reset to zero every frame (and Teddy falls too slow)
		if (Input.GetKeyDown (KeyCode.Space) && onAir == false) {
			onAir = true;
			vel.y = jumpHeight;
			anim.SetTrigger ("Jump");
		} else {
			vel.y = rb.velocity.y;
			//onAir = false;
		}
		 
		rb.velocity = vel;


		transform.Rotate (0, mouseXDelta, 0);

		if (Input.GetButtonDown ("X")) {
			if (OptionsController.controlType == 1) {
				anim.SetTrigger ("Attack");
			}

		} else {
			vel.y = rb.velocity.y;
		}

		if (Input.GetMouseButtonDown (0)) {
			if (OptionsController.controlType == 2) {
				anim.SetTrigger ("Attack");
			}
		} else {
			vel.y = rb.velocity.y;
		}


		if (Input.GetButtonDown ("Y")) {
			if (OptionsController.controlType == 1) {
				Fire ();
			}

		} else {
			vel.y = rb.velocity.y;
		}


		if (Input.GetKeyDown (KeyCode.E)) {
			if (OptionsController.controlType == 2) {
				Fire ();
			}
		} else {
			vel.y = rb.velocity.y;
		}


		if (Input.GetButtonDown ("A")) {
			SceneManager.LoadScene ("SpiritSelectionScene");
			print ("Hello");
			
		}

		//Display hits done on enemy
		if (noOfHits > 0) {
			comboHitText.CrossFadeAlpha (255, 0.05f, true);
			comboHitText.text = noOfHits.ToString () + " x combo";
			comboTimer -= Time.deltaTime;
		}

		//Reset hit on combo timer expiry 
		if (comboTimer <= 0) {
			noOfHits = 0;
			comboHitText.CrossFadeAlpha (0, 0.2f, true);
		}

		//Shake camera if player is hit
		if (shakeCamera) {
			playerCam.transform.localPosition = playerCamOriginalPos + Random.insideUnitSphere * shakeAmount;
			shakeTime -= Time.deltaTime;
		}
        
		if (shakeTime <= 0) {
			shakeCamera = false;
			shakeTime = shakeDuration;
			playerCam.transform.localPosition = playerCamOriginalPos;
		}

		Debug.Log (comboTimer);
	}

	//Function to animate Hit Text
	public void PlayTextAnim ()
	{
		textAnimator.SetTrigger ("popText");
	}


	void Fire ()
	{
		anim.SetTrigger ("Shoot");

		GameObject bullet = (GameObject)Instantiate (bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);

		bullet.GetComponent<Rigidbody> ().velocity = bullet.transform.forward * bulletSpeed;

		Destroy (bullet, bulletLife);
	}

	protected override void Die ()
	{
		base.Die ();
		Invoke ("Respawn", respawnTime);
	}

	void Respawn ()
	{
		anim.SetBool ("Death", false);
		health = startHealth;
		healthBar.sizeDelta = new Vector2 (health * 3, healthBar.sizeDelta.y);
	}


}