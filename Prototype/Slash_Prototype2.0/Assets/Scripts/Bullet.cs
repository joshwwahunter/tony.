﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

    PlayerController playerController;

    public int bulletDamage = 50;

    void Start()
    {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }

	void OnCollisionEnter(Collision collision)
	{
		GameObject hit = collision.gameObject;
		Debug.Log(hit);

        //Enemy hit detection
        if (hit.tag == "Enemy")
        {
            BossHealth health = hit.GetComponent<BossHealth>();
			AIController enemyHealth = hit.GetComponent<AIController> ();

            //Reduce health if health > 0
            if (health != null && health.currentHealth >= 0)
            {
                playerController.noOfHits++;

                //Reset combo timer on each hit 
                playerController.comboTimer = 3f;

                //Play combo text animation & apply damage to boss                
                playerController.PlayTextAnim();
                health.TakeDamage(bulletDamage);
            }

			if (enemyHealth != null && enemyHealth.currentHealth >= 0)
			{
				playerController.noOfHits++;

				//Reset combo timer on each hit 
				playerController.comboTimer = 3f;

				//Play combo text animation & apply damage to boss                
				playerController.PlayTextAnim();
				enemyHealth.TakeDamage(bulletDamage);
			}
        }		

		Destroy(gameObject);
	}
}
