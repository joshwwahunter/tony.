﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ReputationManager : MonoBehaviour
{



	public static ReputationManager instance;

	public static int remReputation;
	public static int setsunaReputation;
	public static int takumiReputation;
	public static int daisukeReputation;
	public static int impression;

	public Sprite rem;
	public Sprite setsuna;
	public Sprite takumi;
	public Sprite daisuke;

	public Image spiritAvatar;
	public static int currentSpirit;

	public Text affinity;
	//public Text remDialogue;

	// Use this for initialization

	void Awake ()
	{

		if (currentSpirit == 1) {
			spiritAvatar.sprite = rem;
		} else if (currentSpirit == 2) {
			spiritAvatar.sprite = setsuna;
		} else if (currentSpirit == 3) {
			spiritAvatar.sprite = takumi;
		} else if (currentSpirit == 4) {
			spiritAvatar.sprite = daisuke;
		}
	}

	void Start ()
	{
		ReputationManager.instance = this;
		//gameObject.GetComponent<Image>;
	}
	
	// Update is called once per frame
	void Update ()
	{
		
		if (currentSpirit == 1) {
			affinity.text = "Affinity: " + remReputation;
		} else if (currentSpirit == 2) {
			affinity.text = "Affinity: " + setsunaReputation;
		} else if (currentSpirit == 3) {
			affinity.text = "Affinity: " + takumiReputation;
		} else if (currentSpirit == 4) {
			affinity.text = "Affinity: " + daisukeReputation;
		}

		if (Input.GetButtonDown ("A")) {
			currentSpirit = 1;
		}
		if (Input.GetButtonDown ("B")) {
			currentSpirit = 2;
		}
		if (Input.GetButtonDown ("X")) {
			currentSpirit = 3;
		}
		if (Input.GetButtonDown ("Y")) {
			currentSpirit = 4;
		}
		if (Input.GetButtonDown ("Start")) {
			BeginConversation ();
		}
	
	}

	/*public void SetImpressionGood ()
	{
		impression = 5;
	}

	public void SetImpressionBad ()
	{
		impression = -5;
	}*/

	public void Impress ()
	{
		if (currentSpirit == 1) {
			remReputation += impression;
		} else if (currentSpirit == 2) {
			setsunaReputation += impression;
		} else if (currentSpirit == 3) {
			takumiReputation += impression;
		} else if (currentSpirit == 4) {
			daisukeReputation += impression;
		}
	}

	public void SetImpressionRem ()
	{
		currentSpirit = 1;

	}

	public void SetImpressionSetsuna ()
	{
		currentSpirit = 2;

	}

	public void SetImpressionTakumi ()
	{
		currentSpirit = 3;

	}

	public void SetImpressionDaisuke ()
	{
		currentSpirit = 4;

	}

	public void BeginConversation ()
	{
		if (currentSpirit > 0) {
			Application.LoadLevel ("DialogueScene");
		}
	}
}








