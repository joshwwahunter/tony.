﻿using UnityEngine;
using System.Collections;

public class BossHealth : MonoBehaviour
{

	public const int maxHealth = 500;

	public int currentHealth = maxHealth;

	public RectTransform healthBar;

    public float deathFallSpeed = 2.0f;

	public void TakeDamage(int amount)
	{
		currentHealth -= amount;

		if (currentHealth <= 0)
		{
			currentHealth = 0;
			Debug.Log("Dead!");

            //Hide Boss healthbar
            healthBar.gameObject.transform.parent.gameObject.SetActive(false);
            Destroy(GetComponent<SphereCollider>());
            Destroy(GetComponent<CapsuleCollider>());

            //Play DeathAnim
            InvokeRepeating("BossDeathAnim", 0, 0.001f);
            BossDeathAnim();			
		}

		healthBar.sizeDelta = new Vector2(currentHealth, healthBar.sizeDelta.y);
	}

    void BossDeathAnim()
    {
        gameObject.transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.AngleAxis(90, Vector3.right), Time.deltaTime * deathFallSpeed);
    }


}
