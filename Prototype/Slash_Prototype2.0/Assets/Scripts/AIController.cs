﻿using UnityEngine;
using System.Collections;

public class AIController : Unit {

	//Enemy variables
	private Animator enemyAnim;
	public float enemySpeed = 7;
	private NavMeshAgent agent;

	//Health Attributes
	public const int maxHealth = 500;
	public int currentHealth = maxHealth;

	//Player detection variables
	public bool playerDetected;
	private bool playerInSight;
	public float enemyFOV = 90f;
	private SphereCollider enemyRangeCollider;
	public LayerMask mask = -1;


	//Stores player location - set to static to keep looking at last location - can be used for implement player search by enemy in future 
	public static GameObject gbjPlayer;

	//variables required for spawning arrows
	public GameObject arrowPrefab;
	private GameObject arrowClone;
	public GameObject arrowSpawner;
	public float arrowSpeed = 10;
	public float arrowlife = 3;
	private bool spawnArrows = false;

	//Offset for arrow spawn location on boss
	public Vector3 offset = new Vector3(0,3,15);

	void Awake()
	{
		enemyAnim = GetComponent<Animator> ();
		enemyRangeCollider = gameObject.GetComponent<SphereCollider>();
		agent = GetComponentInChildren<NavMeshAgent> ();
	}

	// Use this for initialization
	void Start () {

		gameObject.GetComponentInChildren<SkinnedMeshRenderer>().material.SetColor("_Color", Color.blue);
	}

	// Update is called once per frame
	void Update ()
	{

		if (playerDetected)
		{
			gameObject.GetComponentInChildren<SkinnedMeshRenderer>().material.SetColor("_Color", Color.yellow);
			enemyAnim.SetBool ("Proximity", true);
			if (playerInSight)
			{
				gameObject.GetComponentInChildren<SkinnedMeshRenderer>().material.SetColor("_Color", Color.red);
			}
		}
		else
		{
			gameObject.GetComponentInChildren<SkinnedMeshRenderer>().material.SetColor("_Color", Color.blue);
		}
			

	}

	//Player radius enter trigger for Boss
	void OnTriggerStay(Collider other)
	{

		if (other.gameObject.tag == "Player")
		{
			gbjPlayer = other.gameObject;
			playerDetected = true;
			playerInSight = false;
			enemyAnim.SetFloat ("VerticalSpeed", 0);

			// Create a vector from the enemy to the player and store the angle between it and forward.
			Vector3 direction = other.transform.position - transform.position;
			float angle = Vector3.Angle(direction, transform.forward);




			// If the angle between forward and where the player is, is less than half the angle of view...
			if (angle < enemyFOV * 0.5f)
			{
				RaycastHit hit;

				// ... and if a raycast towards the player hits something...
				if (Physics.Raycast(transform.position + transform.up, direction.normalized, out hit, enemyRangeCollider.radius, mask))
				{

					// ... and if the raycast hits the player...
					if (hit.collider.gameObject.tag == "Player")
					{
						Debug.DrawRay(transform.position + transform.up, direction.normalized * 10, Color.red);
						//Debug.DrawLine(transform.position + transform.up, gbjPlayer.transform.position);
						// ... the player is in sight.
						playerInSight = true;

						//Make boss look at player
						gameObject.transform.LookAt(gbjPlayer.transform);

						//Makes the enemy moves towards player
						float step = enemySpeed * Time.deltaTime;
						transform.position = Vector3.MoveTowards (transform.position, gbjPlayer.transform.position, step);
						//enemyAnim.SetFloat ("HorizontalSpeed", 0);
						enemyAnim.SetFloat ("VerticalSpeed", Mathf.Clamp(enemySpeed,0,1));

						//Start spawning arrows by calling the attack iterator
						if (!spawnArrows)
						{
							spawnArrows = true;
							StartCoroutine(AttackPlayer());
						}

					}
				}
			}
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			playerDetected = false;
		}
	}


	//Iterator for ranged attack 
	IEnumerator AttackPlayer()
	{      
		//Spawn arrow every 1 sec
		yield return new WaitForSeconds(1f);

		arrowClone = (GameObject)Instantiate(arrowPrefab, arrowSpawner.transform.position, arrowSpawner.transform.rotation);

		//Shooting arrow along player direction vector
		arrowClone.GetComponent<Rigidbody>().velocity = ((gbjPlayer.transform.position - transform.position).normalized) * arrowSpeed;

		Destroy(arrowClone, arrowlife);

		spawnArrows = false;

		enemyAnim.SetTrigger ("Attack");
	}

	public void TakeDamage(int amount)
	{
		currentHealth -= amount;

		if (currentHealth <= 0)
		{
			currentHealth = 0;
			Debug.Log("Dead!");

			//Hide Boss healthbar
			healthBar.gameObject.transform.parent.gameObject.SetActive(false);
			Destroy(GetComponent<SphereCollider>());
			Destroy(GetComponent<CapsuleCollider>());

			//Play DeathAnim
			enemyAnim.SetTrigger("Death");			
		}

		healthBar.sizeDelta = new Vector2(currentHealth, healthBar.sizeDelta.y);
	}









	/*private Animator enemyanim;
	
	private NavMeshAgent agent;
	public float range = 8;

	private Unit currentEnemy;
	/*private SphereCollider enemyRangeCollider;
	public float enemyFOV;*/

	//public bool playerDetected;
	//private bool playerInsight;

	/*private Vector3 playerPosition;

	private enum State 
	{
		Idle,
		ChasingPlayer
		//AttackingPlayer
	}

	private State currentState;
	private void SetState (State newState)
	{
		currentState = newState;
		//We stop all Coroutines to mke sure we are only in one state at a time
		StopAllCoroutines ();

		switch (currentState) {
		case State.Idle:
			StartCoroutine (OnIdleState ());
			break;
		case State.ChasingPlayer:
			StartCoroutine (OnChasingPlayerState ());
			break;
		/*case State.AttackingPlayer:
			StartCoroutine (OnAttackingPlayerState ());
			break;*/
	/*	}
	}

	/*void Awake () {
		enemyRangeCollider = gameObject.GetComponent<SphereCollider> ();
	}*/

	// Use this for initialization
	/*void Start () {
		agent = GetComponent<NavMeshAgent> ();
		SetState (State.Idle);
		StartCoroutine (OnIdleState ());
	}
	
	IEnumerator OnIdleState ()
	{
		while (true == true) {
			if (currentEnemy == null) {
				LookForEnemies ();
			} else {
				//currentEnemy = PlayerController;
				SetState (State.ChasingPlayer);
			}

			//yield return null makes the coroutine wait for the next frame;
			yield return null;
		}
	}


	IEnumerator OnChasingPlayerState () {
		agent.ResetPath ();


		while (currentEnemy != null) {
			if (currentEnemy.health > 0) 
				{
				agent.SetDestination (currentEnemy.transform.position);
				} else {
				currentEnemy = null;
				SetState (State.Idle);
			}
			yield return null;
		}
	}

	// Update is called once per frame
	void Update () {
		//enemyanim.SetFloat("VerticalSpeed", agent.velocity.magnitude);
	}

	/*void LookForEnemies () {
		Collider[] surroundColliders = Physics.OverlapSphere(transform.position, range);
		foreach (Collider coll in surroundColliders) {
			Unit otherUnit = coll.GetComponent<Unit> ();
			if (otherUnit != null && otherUnit.team != team && otherUnit.health > 0 && CanSee(otherUnit.transform, otherUnit.transform.position + shootOffset)) {
				currentEnemy = otherUnit;
				SetState (State.ChasingEnemy);
				return;
			}
		}
	}*/

	/*void LookForEnemies()
	{
		//if (other.gameObject.tag == "Player")
		//{
			//playerDetected = true;
			//if (playerDetected == true) {
				//currentEnemy = PlayerController;
		Collider[] surroundColliders = Physics.OverlapSphere(transform.position, range);
		foreach (Collider coll in surroundColliders) {
			//currentEnemy = otherUnit;
			SetState (State.ChasingPlayer);
			return;
		}
			//}
		//} 
	}

	public void OnHit (int damageDone) {
		enemyanim.SetTrigger ("Damage");
		health -= damageDone;
		if (health <= 0) {
			Die ();
		}
	}

	protected virtual void Die() {
		enemyanim.SetBool ("Death", true);
		//Destroy (gameObject);
	}*/

}
