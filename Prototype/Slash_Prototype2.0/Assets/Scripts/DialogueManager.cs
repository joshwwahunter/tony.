﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
	public Text dialogue;
	public Text option1;
	public Text option2;
	public Text option3;

	public Button button1;
	public Button button2;
	public Button button3;

	public int dialogueCounter;
	public int currentDialoguePath;
	public int previousChoice;
	public int repEqualizer = 1;

	public int countToFour = 1;

	public TextAsset script;

	string[] dialogueLines;
	int lineNumber;
	public Text uiText;
	Canvas canvas;

	string dialogueString;

	public bool firstChoice = true;

	public ReputationManager other;

	// Use this for initialization
	void Start ()
	{

		if (script) {
			dialogueLines = (script.text.Split ("\n" [0]));
		}

		//dialogueCounter++;
		dialogueString = dialogueLines [lineNumber];

		if (lineNumber < 0) {
			lineNumber = 0;
		}
		if (lineNumber == 0) {
			dialogueString = dialogueLines [lineNumber];
			dialogue.text = dialogueString;
			lineNumber++;
		}
		if (lineNumber == 1) {
			dialogueString = dialogueLines [lineNumber];
			option1.text = dialogueString;
			lineNumber++;
		}

		if (lineNumber == 2) {
			dialogueString = dialogueLines [lineNumber];
			option2.text = dialogueString;
			lineNumber++;
		}

		if (lineNumber == 3) {
			dialogueString = dialogueLines [lineNumber];
			option3.text = dialogueString;
			lineNumber++;
		}
	}
	
	// Update is called once per frame
	void Update ()
	{

		if (Input.GetButtonDown ("A")) {
			button1.onClick.Invoke ();
		}
		if (Input.GetButtonDown ("B")) {
			button2.onClick.Invoke ();
		}
		if (Input.GetButtonDown ("X")) {
			button3.onClick.Invoke ();
		}


		if (repEqualizer == dialogueCounter) {
			other.Impress ();


			repEqualizer++;
		}

		if (dialogueCounter == 1) {
			if (currentDialoguePath == 1) {
				if (firstChoice) {
					ReputationManager.remReputation += 5;
					ReputationManager.impression = 5;
					firstChoice = false;
				}
				if (countToFour == 1) {
					lineNumber = 5;
					dialogueString = dialogueLines [lineNumber];
					dialogue.text = dialogueString;

					countToFour = 2;
				}
				if (countToFour == 2) {
					lineNumber = 6;
					dialogueString = dialogueLines [lineNumber];
					option1.text = dialogueString;

					countToFour = 3;
				}
				if (countToFour == 3) {
					lineNumber = 7;
					dialogueString = dialogueLines [lineNumber];
					option2.text = dialogueString;

					countToFour = 4;
				}
				if (countToFour == 4) {
					lineNumber = 8;
					dialogueString = dialogueLines [lineNumber];
					option2.text = dialogueString;

					countToFour = 1;
				}

			} else if (currentDialoguePath == 2) {
			
				if (firstChoice) {
					firstChoice = false;
				}
				dialogue.text = "get the out of here you son of a gun";
				option1.text = "bloopy";
				option2.text = "blippy";
				option3.text = "blappy";
			} else if (currentDialoguePath == 3) {
				if (firstChoice) {
					//ReputationManager.remReputation -= 5;
					ReputationManager.impression = -5;
					firstChoice = false;
				}
				dialogue.text = "jeb is a mess";
				option1.text = "jeb is a waste";
				option2.text = "jeb is a big fat mess";
				option3.text = "make america great again";

			}


			dialogueCounter++;
		}

	
	}

	/*public void SetRemDialogue ()
	{
		if (currentDialoguePath == 1) {

			if (dialogueCounter == 2) {
				ReputationManager.impression = 5;
				if (previousChoice == 1) {
					ReputationManager.impression = -5;
					dialogue.text = "that was rude of you";
					option1.text = "But I want to";
					option2.text = "all your base are belong to us";
					option3.text = "this is getting difficult";
				} else if (previousChoice == 2) {
					ReputationManager.impression = +5;
					dialogue.text = "you are welcome :)";
					option1.text = "indeedy";
					option2.text = "uh-huh";
					option3.text = "aw ya";
				} else if (previousChoice == 3) {
					ReputationManager.impression = +5;
					dialogue.text = "jeb is a big fat mess";
					option1.text = "vote trump";
					option2.text = "vote trump";
					option3.text = "vote trump";
				}
			}
			if (dialogueCounter > 2) {
				ReputationManager.impression = 5;
				if (previousChoice == 1) {
					dialogue.text = "You really don't have to";
					option1.text = "But I want to";
					option2.text = "all your base are belong to us";
					option3.text = "this is getting difficult";
				} else if (previousChoice == 2) {
					dialogue.text = "yep";
					option1.text = "indeedy";
					option2.text = "uh-huh";
					option3.text = "aw ya";
				} else if (previousChoice == 3) {
					dialogue.text = "jeb is a big fat mess";
					option1.text = "vote trump";
					option2.text = "vote trump";
					option3.text = "vote trump";
				}

			}
		}


		if (currentDialoguePath == 2) {
			
			if (dialogueCounter == 2) {
				ReputationManager.impression = 5;
				dialogue.text = "this is top quality test dialogue";
				option1.text = "yep";
				option2.text = "boy howdy";
				option3.text = "it sure am";
			}
			if (dialogueCounter > 2) {
				ReputationManager.impression = 5;
				dialogue.text = "YOU HAVE EXCEEDED MY CONVERSATIONAL CAPACITY";
				option1.text = "I'm sorry";
				option2.text = "I won't do it again";
				option3.text = "-_-";
			}
		}

		if (currentDialoguePath == 3) {

			if (dialogueCounter == 2) {
				ReputationManager.impression = 5;
				dialogue.text = "jeb is a mess";
				option1.text = "jeb is a waste";
				option2.text = "jeb is a big fat mess";
				option3.text = "make america great again";
			}
			if (dialogueCounter > 2) {
				ReputationManager.impression = 5;
				dialogue.text = "YOU HAVE EXCEEDED MY CONVERSATIONAL CAPACITY";
				option1.text = "I'm sorry";
				option2.text = "I won't do it again";
				option3.text = "-_-";
			}
		}
		dialogueCounter++;



	}
	*/

	public void SetDialoguePath1 ()
	{
		if (currentDialoguePath == 0) {
			currentDialoguePath = 1;

			if (dialogueCounter == 0) {
				dialogueCounter = 1;
			}
		}
		previousChoice = 1;
	}

	public void SetDialoguePath2 ()
	{
		if (currentDialoguePath == 0) {
			currentDialoguePath = 2;

			if (dialogueCounter == 0) {
				dialogueCounter = 1;
			}
		}
		previousChoice = 2;
	}

	public void SetDialoguePath3 ()
	{
		if (currentDialoguePath == 0) {
			currentDialoguePath = 3;

			if (dialogueCounter == 0) {
				dialogueCounter = 1;
			}
		}
		previousChoice = 3;
	}
}



