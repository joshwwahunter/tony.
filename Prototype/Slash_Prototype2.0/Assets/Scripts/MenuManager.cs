﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{

	//public KeyCode key;

	public Button button1;
	public Button button2;
	public Button button3;
	public Button button4;

	Graphic targetGraphic;
	Color normalColor;

	public int currentButton = 1;

	public float buttonTimer;
	public float buttonTime = 0.2f;

	void Awake ()
	{
		//button = GetComponent<Button> ();
		//button.interactable = false;
		targetGraphic = GetComponent<Graphic> ();

		//ColorBlock cb = button.colors;
		//cb.disabledColor = cb.normalColor;
		//button.colors = cb;
	}

	void Start ()
	{

		//button.targetGraphic = null;
		Up ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetButtonDown ("A")) {
			Down ();
		} else if (Input.GetKeyUp (KeyCode.A)) {
			Up ();
		}


		if (Input.GetButtonDown ("X")) {
			currentButton++;

			if (currentButton > 4) {
				currentButton = 0;
			}
		}


		if (currentButton == 1) {
			button1.image.color = Color.green;
			button2.image.color = Color.white;
			button3.image.color = Color.white;
			button4.image.color = Color.white;
		} else if (currentButton == 2) {
			button2.image.color = Color.green;
			button1.image.color = Color.white;
			button3.image.color = Color.white;
			button4.image.color = Color.white;
		} else if (currentButton == 3) {
			button3.image.color = Color.green;
			button2.image.color = Color.white;
			button1.image.color = Color.white;
			button4.image.color = Color.white;
		} else if (currentButton == 4) {
			button4.image.color = Color.green;
			button2.image.color = Color.white;
			button3.image.color = Color.white;
			button1.image.color = Color.white;
		}
	}


	public void LoadGameScene ()
	{
		SceneManager.LoadScene ("GameScene");
	}

	public void LoadOptionsScene ()
	{
		SceneManager.LoadScene ("Options");
	}

	void Up ()
	{
		//StartColorTween (button.colors.normalColor, false);

		if (currentButton == 1) {

			if (buttonTimer < buttonTime) {
				buttonTimer += Time.deltaTime;
			} else if (buttonTimer >= buttonTimer) {
				button1.image.color = Color.white;
				buttonTimer = 0;
			}

		} else if (currentButton == 2) {
			button2.image.color = Color.white;

			if (buttonTimer < buttonTime) {
				buttonTimer += Time.deltaTime;
			} else if (buttonTimer >= buttonTimer) {
				button1.image.color = Color.white;
				buttonTimer = 0;
			}

		} else if (currentButton == 3) {
			button3.image.color = Color.white;

			if (buttonTimer < buttonTime) {
				buttonTimer += Time.deltaTime;
			} else if (buttonTimer >= buttonTimer) {
				button1.image.color = Color.white;
				buttonTimer = 0;
			}

		} else if (currentButton == 4) {
			button4.image.color = Color.white;
		}

		if (buttonTimer < buttonTime) {
			buttonTimer += Time.deltaTime;
		} else if (buttonTimer >= buttonTimer) {
			button1.image.color = Color.white;
			buttonTimer = 0;
		}

	}

	void Down ()
	{
		if (currentButton == 1) {
			button1.image.color = Color.red;
			button1.onClick.Invoke ();
		} else if (currentButton == 2) {
			button1.image.color = Color.red;
			button2.onClick.Invoke ();
		} else if (currentButton == 3) {
			button1.image.color = Color.red;
			button3.onClick.Invoke ();
		} else if (currentButton == 4) {
			button1.image.color = Color.red;
			button4.onClick.Invoke ();
		}


	}



}
