﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class OptionsController : MonoBehaviour
{

	public static int controlType = 1;

	void Update ()
	{
		if (Input.GetButtonDown ("B")) {
			LoadMainMenu ();
		}
	}

	public void ChangeControlType ()
	{
		if (controlType == 2) {
			controlType = 1;
		} else if (controlType == 1) {
			controlType = 2;
		}
	}

	public void LoadMainMenu ()
	{
		SceneManager.LoadScene ("MainMenu");
	}
}
