﻿using UnityEngine;
using System.Collections;

public class AIController : Unit {

	//Enemy variables
	private Animator enemyAnim;
	public float enemySpeed;
	private NavMeshAgent agent;

	//Health Attributes
	public const int maxHealth = 500;
	public int currentHealth = maxHealth;

	//Player detection variables
	public bool playerDetected;
	private bool playerInSight;
	public float enemyFOV = 90f;

	//private SphereCollider enemyRangeCollider;
	public LayerMask mask = -1;
    public float enemyRange = 7.0f;
    public Collider[] colliders;

    public bool isAlive;


	//Stores player location - set to static to keep looking at last location - can be used for implement player search by enemy in future 
	public static GameObject gbjPlayer;

	//variables required for spawning arrows
	public GameObject arrowPrefab;
	private GameObject arrowClone;
	public GameObject arrowSpawner;
	public float arrowSpeed = 10;
	public float arrowlife = 3;
	private bool spawnArrows = false;

	//Offset for arrow spawn location on boss
	public Vector3 offset = new Vector3(0,3,15);

	void Awake()
	{
		enemyAnim = GetComponent<Animator> ();
		agent = GetComponentInChildren<NavMeshAgent> ();
    }

	// Use this for initialization
	void Start ()
    {
        isAlive = true;
        gameObject.GetComponentInChildren<SkinnedMeshRenderer>().material.SetColor("_Color", Color.blue);
        colliders = GetComponents<Collider>();
    }

	// Update is called once per frame
	void Update ()
	{
        if (!isAlive)
        {
            enemyAnim.SetFloat("VerticalSpeed", 0);            
            return;
        }
        LookForPlayer();

		if (playerDetected)
		{
			//enemyAnim.SetBool ("Proximity", true);
			gameObject.GetComponentInChildren<SkinnedMeshRenderer>().material.SetColor("_Color", Color.yellow);
			if (playerInSight)
			{
				gameObject.GetComponentInChildren<SkinnedMeshRenderer>().material.SetColor("_Color", Color.red);
			}
		}
		else
		{
			gameObject.GetComponentInChildren<SkinnedMeshRenderer>().material.SetColor("_Color", Color.blue);
		}
			

	}

    void LookForPlayer()
    {
        Collider[] surroundingColliders = Physics.OverlapSphere(transform.position, enemyRange);
        foreach (Collider coll in surroundingColliders)
        {
            PlayerController playerController = coll.GetComponent<PlayerController>();
            if (playerController != null)
            {
                gbjPlayer = playerController.gameObject;
                playerDetected = true;
                playerInSight = false;
                enemyAnim.SetFloat("VerticalSpeed", 0);

                Vector3 direction = gbjPlayer.transform.position - transform.position;
                float angle = Vector3.Angle(direction, transform.forward);

                // If the angle between forward and where the player is, is less than half the angle of view...
                if (angle < enemyFOV * 0.5f)
                {
                    Debug.DrawRay(transform.position + transform.up, direction.normalized * 10, Color.red);
                    // ... the player is in sight.
                    playerInSight = true;

                    //Make boss look at player
                    gameObject.transform.LookAt(gbjPlayer.transform);

                    //Makes the enemy moves towards player
                    float step = enemySpeed * Time.deltaTime;
                    transform.position = Vector3.MoveTowards(transform.position, gbjPlayer.transform.position, step);
                    enemyAnim.SetFloat("VerticalSpeed", Mathf.Clamp(enemySpeed, 0, 1));

                    //Start spawning arrows by calling the attack iterator
                    if (!spawnArrows && isAlive)
                    {
                        spawnArrows = true;
                        StartCoroutine(AttackPlayer());
                    }
                }               
            }
            else
            {
                //Stop enemy movement if player is out of range
                playerDetected = false;
                enemyAnim.SetFloat("VerticalSpeed", 0);
            }

        }
    }

    /* Disabled to give way to new player detection functionality*/
    //Player radius enter trigger for Boss -
    //void OnTriggerStay(Collider other)
    //{

    //	if (other.gameObject.tag == "Player")
    //	{
    //		gbjPlayer = other.gameObject;
    //		playerDetected = true;
    //		playerInSight = false;
    //		enemyAnim.SetFloat ("VerticalSpeed", 0);

    //		// Create a vector from the enemy to the player and store the angle between it and forward.
    //		Vector3 direction = other.transform.position - transform.position;
    //		float angle = Vector3.Angle(direction, transform.forward);

    //		// If the angle between forward and where the player is, is less than half the angle of view...
    //		if (angle < enemyFOV * 0.5f)
    //		{
    //			RaycastHit hit;

    //               // ... and if a raycast towards the player hits something...
    //               if (Physics.Raycast(transform.position + transform.up, direction.normalized, out hit, enemyRangeCollider.radius, mask))
    //               {

    //                   // ... and if the raycast hits the player...
    //                   if (hit.collider.gameObject.tag == "Player")
    //                   {
    //                       Debug.DrawRay(transform.position + transform.up, direction.normalized * 10, Color.red);
    //                       //Debug.DrawLine(transform.position + transform.up, gbjPlayer.transform.position);
    //                       // ... the player is in sight.
    //                       playerInSight = true;

    //                       //Make boss look at player
    //                       gameObject.transform.LookAt(gbjPlayer.transform);

    //                       //Makes the enemy moves towards player
    //                       float step = enemySpeed * Time.deltaTime;
    //                       transform.position = Vector3.MoveTowards(transform.position, gbjPlayer.transform.position, step);
    //                       //enemyAnim.SetFloat ("HorizontalSpeed", 0);
    //                       enemyAnim.SetFloat("VerticalSpeed", Mathf.Clamp(enemySpeed, 0, 1));

    //                       //Start spawning arrows by calling the attack iterator
    //                       if (!spawnArrows)
    //                       {
    //                           spawnArrows = true;
    //                           StartCoroutine(AttackPlayer());
    //                       }

    //                   }
    //               }
    //           }
    //	}
    //}

    void OnTriggerExit(Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			playerDetected = false;
		}
	}


	//Iterator for ranged attack 
	IEnumerator AttackPlayer()
	{      
		//Spawn arrow every 1 sec
		yield return new WaitForSeconds(1f);

        enemyAnim.SetTrigger("Attack");

        arrowClone = (GameObject)Instantiate(arrowPrefab, arrowSpawner.transform.position, arrowSpawner.transform.rotation);

		//Shooting arrow along player direction vector
		arrowClone.GetComponent<Rigidbody>().velocity = ((gbjPlayer.transform.position - transform.position).normalized) * arrowSpeed;

		Destroy(arrowClone, arrowlife);

		spawnArrows = false;
	}

	public void TakeDamage(int amount)
	{
		currentHealth -= amount;

		if (currentHealth <= 0)
		{
			currentHealth = 0;
            isAlive = false;
            Debug.Log("Dead!");

			//Hide Boss healthbar
			healthBar.gameObject.transform.parent.gameObject.SetActive(false);
            Destroy(GetComponent<SphereCollider>());
            foreach (Collider col in colliders)
            {
                Destroy(col);
            }

            //Play DeathAnim
            enemyAnim.SetTrigger("Death");



        }

		healthBar.sizeDelta = new Vector2(currentHealth, healthBar.sizeDelta.y);
	}

}
