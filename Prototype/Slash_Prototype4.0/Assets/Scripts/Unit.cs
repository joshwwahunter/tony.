﻿using UnityEngine;
using System.Collections;

public class Unit : MonoBehaviour
{

	public int health = 100;
	public int damage = 10;
	public float speed;
	public bool isProtected = false;
	public int protectionValue;

	//Store player healthbar
	public RectTransform healthBar;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	public void OnHit (int damageDone)
	{
		if (isProtected) {
			Protect (protectionValue);
		}
		health -= damageDone;
		healthBar.sizeDelta = new Vector2 (health * 3, healthBar.sizeDelta.y);
		if (health <= 0) {
			Die ();
		}
	}

	public void Protect (int protectionValue)
	{
		health += protectionValue;
		healthBar.sizeDelta = new Vector2 (health * 3, healthBar.sizeDelta.y);
		print ("Hi");
	}

	protected virtual void Die ()
	{
		//anim.SetBool("Death", true);
		Debug.Log ("You Died!");
	}


}
