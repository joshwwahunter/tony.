﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//using UnityEditor;

public class DialogueManager : MonoBehaviour
{
	public Text dialogue;
	public Text option1;
	public Text option2;
	public Text option3;

	public Button button1;
	public Button button2;
	public Button button3;
	public bool firstPress = true;

	public int dialogueCounter;
	public int currentDialoguePath;
	public int previousChoice;
	public int repEqualizer = 1;
	public int currentCounter = 1;
	public int scene = 1;
	public int currentState = 1;
	public int newScene;
	public int currentButton;

	public int countToFour = 1;

	public bool shouldBeDoingThis = true;

	public Image start;
	public Sprite avatar;

	// All useable scripts, hopefully to be condensed into one document
	TextAsset currentScript;
	public TextAsset script1;
	public TextAsset script2;
	public TextAsset script3;
	public TextAsset script4;
	public TextAsset script5;
	public TextAsset script6;
	public TextAsset script7;
	public TextAsset script8;
	/*public TextAsset script9;
	public TextAsset script10;
	public TextAsset script11;*/

	public int choiceStrength;
	static public bool dialogueHappening = false;


	public ReputationManager rm;

	string[] dialogueLines;
	int lineNumber;
	int lineNumber2;
	int lineNumber3;
	public Text uiText;
	Canvas canvas;
	public int stringLength;

	string dialogueString;

	public bool firstChoice = true;

	public ReputationManager other;

	// Use this for initialization

	void Awake ()
	{
		dialogueHappening = true;
		SetDialouge ();
		start.enabled = false;
		if (ReputationManager.currentSpirit == 0) {
			ReputationManager.currentSpirit = 2;
		}
	}

	void Start ()
	{
		
		

		dialogueLines = (currentScript.text.Split ("\n" [0]));


		

		//dialogueCounter++;
		dialogueString = dialogueLines [lineNumber];

		if (lineNumber < 0) {
			lineNumber = 0;
		}
		if (lineNumber == 0) {
			dialogueString = dialogueLines [lineNumber];
			dialogue.text = dialogueString;
			lineNumber++;
		}
		if (lineNumber == 1) {
			dialogueString = dialogueLines [lineNumber];
			option1.text = dialogueString;
			lineNumber++;
		}

		if (lineNumber == 2) {
			dialogueString = dialogueLines [lineNumber];
			option2.text = dialogueString;
			lineNumber++;
		}

		if (lineNumber == 3) {
			dialogueString = dialogueLines [lineNumber];
			option3.text = dialogueString;
			lineNumber++;
		}
	}



	
	// Update is called once per frame
	void Update ()
	{


		if (Input.GetButtonDown ("A")) {
			if (currentCounter >= 8) {
				SceneManager.LoadScene ("MainMenu");
			}
		}
		if (Input.GetKey (KeyCode.A)) {
			SceneManager.LoadScene ("MainMenu");
		}
		if (Input.GetButtonDown ("B")) {
			
			currentButton = 2;
			button2.image.color = Color.green;
			button1.image.color = Color.white;
			start.enabled = true;
			//button2.onClick.Invoke ();
		}


		if (Input.GetButtonDown ("X")) {
			
			currentButton = 1;
			button1.image.color = Color.green;
			button2.image.color = Color.white;
			start.enabled = true;
			
			//button1.onClick.Invoke ();
		}

		if (Input.GetButtonDown ("Start")) {
			if (currentButton == 1) {
				button1.onClick.Invoke ();
			} else if (currentButton == 2) {
				button2.onClick.Invoke ();
			}
			button1.image.color = Color.white;
			button2.image.color = Color.white;
			start.enabled = false;
		}



		if (repEqualizer == dialogueCounter) {
			other.Impress ();


			repEqualizer++;
		}

	


		if (dialogueCounter == 1) {
			if (currentDialoguePath == 1) {
				if (firstChoice) {
					ReputationManager.remReputation += 5;
					ReputationManager.impression = 5;
					firstChoice = false;
				}


			}

		}
	}



	public void SetDialoguePath1 ()
	{
		/*if (currentCounter >= 7) {
			SceneManager.LoadScene ("MainMenu");
		}*/
		SetState ();
		shouldBeDoingThis = true;
		//if (currentDialoguePath == 0) {
		currentDialoguePath = 1;

		if (dialogueCounter == 0) {
			dialogueCounter = 1;
			//}


		}
		Impress ();

		SetRoute ();

		previousChoice = 1;
		SetDialouge ();
		SetImpression1 ();
	}

	public void SetDialoguePath2 ()
	{
		/*if (currentCounter >= 7) {
			SceneManager.LoadScene ("MainMenu");
		}*/
		SetState ();
		shouldBeDoingThis = true;
		//if (currentDialoguePath == 0) {
		currentDialoguePath = 2;

		if (dialogueCounter == 0) {
			dialogueCounter = 1;
			//}
		}

		Impress ();

		SetRoute ();
		


		previousChoice = 2;
		SetDialouge ();
		SetImpression1 ();
	}

	public void SetDialoguePath3 ()
	{
		if (currentCounter >= 9) {
			SceneManager.LoadScene ("MainMenu");
		}
	
		shouldBeDoingThis = true;
		if (currentDialoguePath == 0) {
			currentDialoguePath = 3;

			if (dialogueCounter == 0) {
				dialogueCounter = 1;
			}
		}

		Impress ();
		SetRoute ();

		previousChoice = 3;
		SetDialouge ();
	}

	public void SetDialouge ()
	{
		//currentState = 2;
		if (currentCounter == 1) {
			currentScript = script1;
			if (previousChoice == 1) {
				currentState = 2;
			} else if (previousChoice == 2) {
				currentState = 5;
			}
		} else if (currentCounter == 2) {
			currentScript = script2;
			if (previousChoice == 1) {
				currentState = 4;
			} else if (previousChoice == 2) {
				currentState = 7;
			}
		} else if (currentCounter == 3) {
			currentScript = script3;
			if (previousChoice == 1) {
				currentState = 2;
			} else if (previousChoice == 2) {
				currentState = 1;
			}
		} else if (currentCounter == 4) {
			currentScript = script4;
			if (scene == 1) {
				if (previousChoice == 1) {
					currentState = 6;
				} else if (previousChoice == 2) {
					currentState = 7;
				}
			}
			if (scene == 2) {
				if (previousChoice == 1) {
					currentState = 3;
				} else if (previousChoice == 2) {
					currentState = 4;
				}
			}
		} else if (currentCounter == 5) {
			currentScript = script5;
			if (previousChoice == 1) {

			} else if (previousChoice == 2) {

			}
		} else if (currentCounter == 6) {
			currentScript = script6;
			if (previousChoice == 1) {

			} else if (previousChoice == 2) {

			}
		} else if (currentCounter == 7) {
			currentScript = script7;
			if (previousChoice == 1) {

			} else if (previousChoice == 2) {

			}
		} else if (currentCounter >= 8) {
			currentScript = script8;
			if (previousChoice == 1) {

			} else if (previousChoice == 2) {

			}
		} /*else if (currentCounter == 9) {
			currentScript = script9;
			if (previousChoice == 1) {

			} else if (previousChoice == 2) {

			}
		} else if (currentCounter == 10) {
			currentScript = script10;
			if (previousChoice == 1) {

			} else if (previousChoice == 2) {

			}
		} else if (currentCounter == 11) {
			currentScript = script11;
			if (previousChoice == 1) {

			} else if (previousChoice == 2) {

			}
		}*/
		currentCounter++;
		dialogueLines = (currentScript.text.Split ("\n" [0]));
		//scene++;
		Voidy ();

	}

	public void Voidy ()
	{
		

		if (countToFour == 1) {
			SetLineNumber ();
			dialogueString = dialogueLines [lineNumber];
			stringLength = dialogueString.Length;


			countToFour = 2;
		}
		if (countToFour == 2) {
			SetLineNumber ();
			dialogueString = dialogueLines [lineNumber];
			option1.text = dialogueString;

			countToFour = 3;
		}
		if (countToFour == 3) {
			SetLineNumber ();
			dialogueString = dialogueLines [lineNumber];
			option2.text = dialogueString;

			countToFour = 1;
		}
		dialogueCounter++;


	}

	public void SetLineNumber ()
	{
		if (scene == 1) {
			if (countToFour == 1) {
				lineNumber = 0;
			}
			if (countToFour == 2) {
				lineNumber = 1;

			}
			if (countToFour == 3) {
				lineNumber = 2;
			}
		} else if (scene == 2) {

			if (countToFour == 1) {
				lineNumber = 3;
			}
			if (countToFour == 2) {
				lineNumber = 4;

			}
			if (countToFour == 3) {
				lineNumber = 5;
			}
		} else if (scene == 3) {
			if (countToFour == 1) {
				lineNumber = 6;
			}
			if (countToFour == 2) {
				lineNumber = 7;

			}
			if (countToFour == 3) {
				lineNumber = 8;
			}
		} else if (scene == 4) {
			if (countToFour == 1) {
				lineNumber = 9;
			}
			if (countToFour == 2) {
				lineNumber = 10;

			}
			if (countToFour == 3) {
				lineNumber = 11;
			}
		} else if (scene == 5) {
			if (countToFour == 1) {
				lineNumber = 12;
			}
			if (countToFour == 2) {
				lineNumber = 13;

			}
			if (countToFour == 3) {
				lineNumber = 14;
			}
		} else if (scene == 6) {
			if (countToFour == 1) {
				lineNumber = 15;
			}
			if (countToFour == 2) {
				lineNumber = 16;

			}
			if (countToFour == 3) {
				lineNumber = 17;
			}
		} else if (scene == 7) {
			if (countToFour == 1) {
				lineNumber = 18;
			}
			if (countToFour == 2) {
				lineNumber = 19;

			}
			if (countToFour == 3) {
				lineNumber = 20;
			}
		} else if (scene == 8) {
			if (countToFour == 1) {
				lineNumber = 21;
			}
			if (countToFour == 2) {
				lineNumber = 22;

			}
			if (countToFour == 3) {
				lineNumber = 23;
			}
		} else if (scene == 9) {
			if (countToFour == 1) {
				lineNumber = 24;
			}
			if (countToFour == 2) {
				lineNumber = 25;

			}
			if (countToFour == 3) {
				lineNumber = 26;
			}
		} else if (scene == 10) {
			if (countToFour == 1) {
				lineNumber = 27;
			}
			if (countToFour == 2) {
				lineNumber = 28;

			}
			if (countToFour == 3) {
				lineNumber = 29;
			}
		} else if (scene == 11) {
			if (countToFour == 1) {
				lineNumber = 30;
			}
			if (countToFour == 2) {
				lineNumber = 31;

			}
			if (countToFour == 3) {
				lineNumber = 32;
			}
		} else if (scene == 12) {
			if (countToFour == 1) {
				lineNumber = 33;
			}
			if (countToFour == 2) {
				lineNumber = 34;

			}
			if (countToFour == 3) {
				lineNumber = 35;
			}
		} else if (scene == 13) {
			if (countToFour == 1) {
				lineNumber = 36;
			}
			if (countToFour == 2) {
				lineNumber = 37;

			}
			if (countToFour == 3) {
				lineNumber = 38;
			}
		} else if (scene == 14) {
			if (countToFour == 1) {
				lineNumber = 39;
			}
			if (countToFour == 2) {
				lineNumber = 40;

			}
			if (countToFour == 3) {
				lineNumber = 41;
			}
		} else if (scene == 15) {
			if (countToFour == 1) {
				lineNumber = 42;
			}
			if (countToFour == 2) {
				lineNumber = 43;

			}
			if (countToFour == 3) {
				lineNumber = 44;
			}
		} else if (scene == 16) {
			if (countToFour == 1) {
				lineNumber = 45;
			}
			if (countToFour == 2) {
				lineNumber = 46;

			}
			if (countToFour == 3) {
				lineNumber = 47;
			}
		} else if (scene == 17) {
			if (countToFour == 1) {
				lineNumber = 48;
			}
			if (countToFour == 2) {
				lineNumber = 49;

			}
			if (countToFour == 3) {
				lineNumber = 50;
			}
		} else if (scene == 18) {
			if (countToFour == 1) {
				lineNumber = 51;
			}
			if (countToFour == 2) {
				lineNumber = 52;

			}
			if (countToFour == 3) {
				lineNumber = 53;
			}
		} else if (scene == 19) {
			if (countToFour == 1) {
				lineNumber = 54;
			}
			if (countToFour == 2) {
				lineNumber = 55;

			}
			if (countToFour == 3) {
				lineNumber = 56;
			}
		} 
	}


	public void SetState ()
	{
		if (ReputationManager.currentSpirit == 2) {
			if (currentState == 1) {
				rm.spiritAvatar.sprite = rm.setsuna_ECGa;
			} else if (currentState == 2) {
				rm.spiritAvatar.sprite = rm.setsuna_ECGB;
			} else if (currentState == 3) {
				rm.spiritAvatar.sprite = rm.setsuna_ECGr;
			} else if (currentState == 4) {
				rm.spiritAvatar.sprite = rm.setsuna_EOF;
			} else if (currentState == 5) {
				rm.spiritAvatar.sprite = rm.setsuna_EOGa;
			} else if (currentState == 6) {
				rm.spiritAvatar.sprite = rm.setsuna_EOGS;
			} else if (currentState == 7) {
				rm.spiritAvatar.sprite = rm.setsuna_EOS;
			}
		} else if (ReputationManager.currentSpirit == 1) {
		} else if (ReputationManager.currentSpirit == 3) {
		} else if (ReputationManager.currentSpirit == 4) {
		}

	}

	public void SetRoute ()
	{
		if (shouldBeDoingThis) {
			if (currentDialoguePath == 1) {

				if (currentCounter == 5) {
					if (scene == 1) {
						scene = 1;
					} else if (scene == 2) {
						scene = 3;
					} else if (scene == 3) {
						scene = 5;
					} else if (scene == 4) {
						scene = 7;
					}
				}
				if (currentCounter == 6) {
					if (scene == 1) {
						scene = 1;
					} else if (scene == 2) {
						scene = 3;
					} else if (scene == 3) {
						scene = 5;
					} else if (scene == 4) {
						scene = 7;
					} else if (scene == 5) {
						scene = 9;
					} else if (scene == 6) {
						scene = 11;
					} else if (scene == 7) {
						scene = 13;
					} else if (scene == 8) {
						scene = 15;
					}
				}

				if (currentCounter == 7) {
					if (scene == 1) {
						scene = 1;
					} else if (scene == 2) {
						scene = 1;
					} else if (scene == 3) {
						scene = 3;
					} else if (scene == 4) {
						scene = 5;
					} else if (scene == 5) {
						scene = 7;
					} else if (scene == 6) {
						scene = 9;
					} else if (scene == 7) {
						scene = 9;
					} else if (scene == 8) {
						scene = 3;
					} else if (scene == 9) {
						scene = 9;
					} else if (scene == 10) {
						scene = 3;
					} else if (scene == 11) {
						scene = 11;
					} else if (scene == 12) {
						scene = 11;
					} else if (scene == 13) {
						scene = 9;
					} else if (scene == 15) {
						scene = 16;
					} else if (scene == 16) {
						scene = 18;
					}
				}

				if (currentCounter >= 8) {
					if (shouldBeDoingThis) {
						if (scene == 1) {
							scene = 12;
							shouldBeDoingThis = false;
						} else if (scene == 2) {
							scene = 12;
							shouldBeDoingThis = false;
						} else if (scene == 3) {
							scene = 19;
							SetNewScene ();
							shouldBeDoingThis = false;
						} else if (scene == 4) {
							scene = 12;
							SetNewScene ();
							shouldBeDoingThis = false;
						} else if (scene == 5) {
							scene = 6;
							SetNewScene ();
							currentCounter++;
						} else if (scene == 6) {
							scene = 8;
							SetNewScene ();
							shouldBeDoingThis = false;
						} else if (scene == 7) {
							scene = 4;
							SetNewScene ();
							shouldBeDoingThis = false;
						} else if (scene == 8) {
							shouldBeDoingThis = false;
						} else if (scene == 12) {
							scene = 10;
							SetNewScene ();
							shouldBeDoingThis = false;
						} else if (scene == 13) {
							scene = 10;
							SetNewScene ();
							shouldBeDoingThis = false;
						} else if (scene == 14) {
							scene = 17;
							SetNewScene ();
							shouldBeDoingThis = false;
						} else if (scene == 15) {
							scene = 12;
							SetNewScene ();
							shouldBeDoingThis = false;
						} else if (scene == 16) {
							scene = 12;
							SetNewScene ();
							shouldBeDoingThis = false;
						} else if (scene == 17) {
							scene = 15;
							SetNewScene ();
							shouldBeDoingThis = false;
						} else if (scene == 18) {
							scene = 15;
							SetNewScene ();
							shouldBeDoingThis = false;
						} else if (scene == 19) {
							scene = 12;
							SetNewScene ();
							shouldBeDoingThis = false;
						}
					}
				}

			}
		
		}
			
	



		if (currentDialoguePath == 2) {
			if (currentCounter == 4) {
				scene = 2;
			}
			if (currentCounter == 5) {
				if (scene == 1) {
					scene = 2;
				} else if (scene == 2) {
					scene = 4;
				}
			}
			if (currentCounter == 6) {
				if (scene == 1) {
					scene = 2;
				} else if (scene == 2) {
					scene = 4;
				} else if (scene == 3) {
					scene = 6;
				} else if (scene == 4) {
					scene = 8;
				}
			}

			if (currentCounter == 7) {
				if (scene == 1) {
					scene = 2;
				} else if (scene == 2) {
					scene = 2;
				} else if (scene == 3) {
					scene = 4;
				} else if (scene == 4) {
					scene = 6;
				} else if (scene == 5) {
					scene = 1;
				} else if (scene == 6) {
					scene = 15;
				} else if (scene == 7) {
					scene = 15;
				} else if (scene == 8) {
					scene = 8;
				} else if (scene == 9) {
					scene = 15;
				} else if (scene == 10) {
					scene = 4;
				} else if (scene == 11) {
					scene = 11;
				} else if (scene == 12) {
					scene = 11;
				} else if (scene == 13) {
					scene = 15;
				} else if (scene == 15) {
					scene = 16;
				} else if (scene == 16) {
					scene = 19;
				}
			}

			if (currentCounter >= 8) {
				if (shouldBeDoingThis) {
					if (scene == 1) {
						scene = 13;

						shouldBeDoingThis = false;
					} else if (scene == 2) {
						scene = 13;
						shouldBeDoingThis = false;
					} else if (scene == 3) {
						scene = 1;
						shouldBeDoingThis = false;
					} else if (scene == 4) {
						scene = 13;
						shouldBeDoingThis = false;
					} else if (scene == 5) {
						shouldBeDoingThis = false;
					} else if (scene == 6) {
						scene = 14;
						shouldBeDoingThis = false;
					} else if (scene == 7) {
						scene = 4;
						shouldBeDoingThis = false;
					} else if (scene == 8) {
						scene = 16;
						shouldBeDoingThis = false;
					} else if (scene == 9) {
						//scene = 11;
						print ("Hi");
						shouldBeDoingThis = false;
					} else if (scene == 10) {
						//scene = 11;
						print ("Hi");
						shouldBeDoingThis = false;
					} else if (scene == 11) {
						//scene = 11;
						print ("Hi");
						shouldBeDoingThis = false;
					} else if (scene == 12) {
						scene = 11;
						shouldBeDoingThis = false;
					} else if (scene == 13) {
						scene = 11;
						shouldBeDoingThis = false;
					} else if (scene == 14) {
						scene = 18;
						shouldBeDoingThis = false;
					} else if (scene == 15) {
						scene = 13;
						shouldBeDoingThis = false;
					} else if (scene == 16) {
						scene = 13;
						shouldBeDoingThis = false;
					} else if (scene == 17) {
						scene = 16;
						shouldBeDoingThis = false;
					} else if (scene == 18) {
						scene = 16;
						shouldBeDoingThis = false;
					} else if (scene == 19) {
						scene = 13;

					}
				}
			
			}
	
		}

	}


	

	void SetNewScene ()
	{
		//scene = newScene;
		//shouldBeDoingThis = false;
	}

	void Impress ()
	{
		ReputationManager.impression += choiceStrength;
	}

	void SetImpression1 ()
	{
		if (dialogueCounter == 1) {
			if (scene == 1) {
				if (previousChoice == 1) {
					choiceStrength = 10;
				} else if (previousChoice == 2) {
					choiceStrength = -10;
				}

			}
		}

		if (dialogueCounter == 2) {
			if (scene == 1) {
				if (previousChoice == 1) {
					choiceStrength = 0;
				} else if (previousChoice == 2) {
					choiceStrength = -5;
				}

			}
		}

		if (dialogueCounter == 3) {
			if (scene == 1) {
				if (previousChoice == 1) {
					choiceStrength = 5;
				} else if (previousChoice == 2) {
					choiceStrength = 0;
				}

			} 

		
		}

		if (dialogueCounter == 4) {
			if (scene == 1) {
				if (previousChoice == 1) {
					choiceStrength = -5;
				} else if (previousChoice == 2) {
					choiceStrength = 10;
				}

			} else if (scene == 2) {
				if (previousChoice == 1) {
					choiceStrength = 0;
				} else if (previousChoice == 2) {
					choiceStrength = -5;
				}
			}


		
		}

		if (dialogueCounter == 5) {
			if (scene == 1) {
				if (previousChoice == 1) {
					choiceStrength = 0;
				} else if (previousChoice == 2) {
					choiceStrength = -5;
				}

			} else if (scene == 2) {
				if (previousChoice == 1) {
					choiceStrength = 0;
				} else if (previousChoice == 2) {
					choiceStrength = -5;
				}
			} else if (scene == 3) {
				if (previousChoice == 1) {
					choiceStrength = -10;
				} else if (previousChoice == 2) {
					choiceStrength = 10;
				}
			} else if (scene == 4) {
				if (previousChoice == 1) {
					choiceStrength = -10;
				} else if (previousChoice == 2) {
					choiceStrength = 10;
				}
			}


		
		}

		if (dialogueCounter == 6) {
			if (scene == 1) {
				if (previousChoice == 1) {
					choiceStrength = 5;
				} else if (previousChoice == 2) {
					choiceStrength = 0;
				}

			} else if (scene == 2) {
				if (previousChoice == 1) {
					choiceStrength = 0;
				} else if (previousChoice == 2) {
					choiceStrength = -5;
				}
			} else if (scene == 3) {
				if (previousChoice == 1) {
					choiceStrength = 5;
				} else if (previousChoice == 2) {
					choiceStrength = 10;
				}
			} else if (scene == 4) {
				if (previousChoice == 1) {
					choiceStrength = 10;
				} else if (previousChoice == 2) {
					choiceStrength = -10;
				}
			} else if (scene == 5) {
				if (previousChoice == 1) {
					choiceStrength = 10;
				} else if (previousChoice == 2) {
					choiceStrength = -10;
				}
			} else if (scene == 6) {
				if (previousChoice == 1) {
					choiceStrength = 10;
				} else if (previousChoice == 2) {
					choiceStrength = -10;
				}
			} else if (scene == 7) {
				if (previousChoice == 1) {
					choiceStrength = 10;
				} else if (previousChoice == 2) {
					choiceStrength = -10;
				}
			} else if (scene == 8) {
				if (previousChoice == 1) {
					choiceStrength = 10;
				} else if (previousChoice == 2) {
					choiceStrength = -10;
				}
			}



		
		}

		if (dialogueCounter == 7) {
			if (scene == 1) {
				if (previousChoice == 1) {
					choiceStrength = 10;
				} else if (previousChoice == 2) {
					choiceStrength = -10;
				}

			} else if (scene == 2) {
				if (previousChoice == 1) {
					choiceStrength = 10;
				} else if (previousChoice == 2) {
					choiceStrength = -10;
				}
			} else if (scene == 3) {
				if (previousChoice == 1) {
					choiceStrength = 10;
				} else if (previousChoice == 2) {
					choiceStrength = -10;
				}
			} else if (scene == 4) {
				if (previousChoice == 1) {
					choiceStrength = 10;
				} else if (previousChoice == 2) {
					choiceStrength = -10;
				}
			} else if (scene == 5) {
				if (previousChoice == 1) {
					choiceStrength = 10;
				} else if (previousChoice == 2) {
					choiceStrength = -10;
				}
			} else if (scene == 6) {
				if (previousChoice == 1) {
					choiceStrength = 10;
				} else if (previousChoice == 2) {
					choiceStrength = -10;
				}
			} else if (scene == 7) {
				if (previousChoice == 1) {
					choiceStrength = 10;
				} else if (previousChoice == 2) {
					choiceStrength = -10;
				}
			} else if (scene == 8) {
				if (previousChoice == 1) {
					choiceStrength = 10;
				} else if (previousChoice == 2) {
					choiceStrength = -10;
				}
			} else if (scene == 9) {
				if (previousChoice == 1) {
					choiceStrength = 10;
				} else if (previousChoice == 2) {
					choiceStrength = -10;
				}
			} else if (scene == 10) {
				if (previousChoice == 1) {
					choiceStrength = 10;
				} else if (previousChoice == 2) {
					choiceStrength = -10;
				}
			} else if (scene == 11) {
				if (previousChoice == 1) {
					choiceStrength = 10;
				} else if (previousChoice == 2) {
					choiceStrength = -10;
				}
			} else if (scene == 12) {
				if (previousChoice == 1) {
					choiceStrength = 10;
				} else if (previousChoice == 2) {
					choiceStrength = -10;
				}
			} else if (scene == 13) {
				if (previousChoice == 1) {
					choiceStrength = 10;
				} else if (previousChoice == 2) {
					choiceStrength = -10;
				}
			} else if (scene == 14) {
				if (previousChoice == 1) {
					choiceStrength = 10;
				} else if (previousChoice == 2) {
					choiceStrength = -10;
				}
			} else if (scene == 15) {
				if (previousChoice == 1) {
					choiceStrength = 10;
				} else if (previousChoice == 2) {
					choiceStrength = -10;
				}
			} else if (scene == 16) {
				if (previousChoice == 1) {
					choiceStrength = 10;
				} else if (previousChoice == 2) {
					choiceStrength = -10;
				}
			}


		
		}





	}






		

	
}

