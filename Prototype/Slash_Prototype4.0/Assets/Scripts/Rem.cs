﻿using UnityEngine;
using System.Collections;

public class Rem : Spirit
{

	public SphereCollider myColl;
	//public float ghostSpeed = 1;
	AIController target;
	//public PlayerController player;
	public bool hasAttacked = false;
	//public bool foundTarget;

	// Use this for initialization
	void Start ()
	{
		myColl.radius = 100;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (target != null) {

			transform.position = Vector3.MoveTowards (transform.position, target.transform.position, ghostSpeed);
			foundTarget = true;
		}
	}

	void OnTriggerEnter (Collider coll)
	{
		if (foundTarget == false) {

			if (player == null) {
				player = coll.GetComponent<PlayerController> ();
			}

			if (target == null) {
				target = coll.GetComponent<AIController> ();
				//print ("Hi");
				myColl.radius = 0.5f;
			}
		}
		if (foundTarget) {
			if (target != null) {
				if (hasAttacked == false) {
					target.health -= 10;
					hasAttacked = true;
				}
			}
		}
	}
}



