﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float speed;
	public float jumpHeight;
	private Rigidbody rb;
	private Transform camContainer;
	private Camera playerCam;
	private Animator anim;

	public int health = 100;
	//public int damage = 10;

	void Awake()
	{
		rb = GetComponent<Rigidbody> ();
		anim = GetComponentInChildren<Animator> ();
	}

	// Use this for initialization
	void Start () {
		playerCam = Camera.main;
		camContainer =playerCam.transform.parent;
	}

	// Update is called once per frame
	void Update () {
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;

		float horizontal = Input.GetAxis ("Horizontal");
		float vertical = Input.GetAxis ("Vertical");

		Vector3 input = new Vector3 (horizontal, 0, vertical);

		//We normalize the input vector, so that it always has a size of 1 (even when both forward and sideways are pressed)
		Vector3 vel = input.normalized * speed;

		anim.SetFloat ("HorizontalSpeed", input.x);
		anim.SetFloat ("VerticalSpeed", input.z);
		//We rotate the vector, so that it returns a global vector, based on the rotation of the character
		vel = transform.TransformVector (vel);
		//We set the vel.y, because otherwise the rigibodies y-velocity is reset to zero every frame (and Teddy falls too slow)
		if (Input.GetKeyDown (KeyCode.Space)) {
			vel.y = jumpHeight;
			anim.SetTrigger ("Jump");
		} else {
			vel.y = rb.velocity.y;
		}

		rb.velocity = vel;

		float mouseXDelta = Input.GetAxis ("Mouse X");
		transform.Rotate (0, mouseXDelta, 0);

		float mouseYDelta = Input.GetAxis ("Mouse Y");
		camContainer.Rotate (-mouseYDelta, 0, 0);

		if (Input.GetMouseButtonDown (0)) {
			anim.SetTrigger ("Attack");
		} else {
			vel.y = rb.velocity.y;
		}
	}
}
