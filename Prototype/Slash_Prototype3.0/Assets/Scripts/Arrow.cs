﻿using UnityEngine;
using System.Collections;

public class Arrow : MonoBehaviour {

    PlayerController playerController;

    public int arrowDamage = 10;

    void Start()
    {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }

	void OnCollisionEnter(Collision collision)
	{
		GameObject hitObject = collision.gameObject;
		Debug.Log(hitObject);

        //Enemy hit detection
        if (hitObject.tag == "Player")
        {
            Unit unit = hitObject.GetComponent<Unit>();

            //Reduce health if health > 0
            if (unit.health >= 0)
            {
                //health.TakeDamage(arrowDamage);
                unit.OnHit(arrowDamage);

                playerController.shakeCamera = true;
                playerController.noOfHits = 0;
                playerController.comboHitText.CrossFadeAlpha(0, 0.2f, true);

            }
        }		

		Destroy(gameObject);
	}
}
